#pragma once

#ifndef WEATHER_STATION_H
#define WEATHER_STATION_H

#include "stdafx.h"
#include <string>
#include <stack>
#include "weatherreading.h"
#include "Language.h"
#include "PropertiesReader.h"


class Weather_Station {

	std::string* stationName;

	Language* language;
	Properties_Reader* properties;
	bool hasSetLanguage;

	int* historySize;
	std::stack <Weather_Reading*> readings;

	public:

		/*
		* Constructor
		*/
		Weather_Station() :stationName(nullptr), hasSetLanguage(false), language(nullptr), historySize(nullptr) {}

		/*
		* Operations
		*/
		bool hasReadings();
		void storeReading(Weather_Reading* newReading);
		int numReadings();
		Weather_Reading* latestReading();
		std::string* getOutputMessage(std::string* messageKey);
		
		/*
		* Getters/Setters
		*/
		int getHistorySize();
		void setHistorySize(int newHistorySize);

		std::string getStationName();
		void setStationName(std::string newStationName);

		Language getLanguage();
		void setLanguage(Language newLanguage);

		std::stack <Weather_Reading*> getReadings();

		//Used for checking if the user has already been automatically prompted to change the language or not
		bool hasLanguagePrompted();
};


#endif
