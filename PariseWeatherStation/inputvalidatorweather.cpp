#include "stdafx.h"
#include "inputvalidatorweather.h"

bool Input_Validator_Weather::isValid_WindDirection(std::string input) {
	if (this->inputValidator->isValid_ContainsOnlyLetters(input)) {
		this->inputValidator->toggleDisplayErrorMessages(false);
		if (!this->inputValidator->isValid_Integer(input, false)) {
			this->inputValidator->toggleDisplayErrorMessages(true);
			if (this->inputValidator->isValid_Direction(input)) {
				return true;
			}
		}
		else {
			this->inputValidator->toggleDisplayErrorMessages(true);
		}
	}

	return false;
}

bool Input_Validator_Weather::isValid_StationName(std::string input) {
	if (this->inputValidator->isValid_NonEmptyString(input)) {
		return true;
	}
	return false;
}

bool Input_Validator_Weather::isValid_TempDegrees(std::string input) {
	if (this->inputValidator->isValid_Integer(input, true)) {
		return true;
	}
	return false;
}

bool Input_Validator_Weather::isValid_WindSpeed(std::string input) {
	if (this->inputValidator->isValid_UnsignedInt(input)) {
		return true;
	}
	return false;
}
