#pragma once
#ifndef LANGUAGE_H
#define LANGUAGE_H

#include "stdafx.h"
#include <string>
#include "PropertiesReader.h"

	enum Language { English = 1, Russian};

	//To String
	static const char * LanguageStrings[] = { "English", "Russian" };
	const char * getTextForLanguage(int langVal);

	//For locating Translation Data File
	static const char * DataFileStrings[] = { "English.dat", "Russian.dat" };
	const char * getDataFileForLanguage(int langVal);

#endif