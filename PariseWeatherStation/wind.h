#pragma once

#ifndef WIND_H
#define WIND_H

#include "stdafx.h"
#include <string>

class Wind {

	std::string* windDir;
	int* windSpeed;

	public:

		/*
		* Constructor
		*/
		Wind() : windDir(nullptr), windSpeed(nullptr) {};
		
		/*
		* Getters/Setters
		*/

		int getWindSpeed();
		void setWindSpeed(int newWindSpeed);

		std::string getWindDirection();
		void setWindDirection(std::string newWindDirection);

};


#endif
