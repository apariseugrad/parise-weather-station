#pragma once

#ifndef INPUT_VALIDATOR_WEATHER_H
#define INPUT_VALIDATOR_WEATHER_H

#include "stdafx.h"
#include <string>
#include "inputvalidatorbase.h"

/*
	Encapsulation for weather input validation
 */

class Input_Validator_Weather {
	Input_Validator_Base* inputValidator;
	
	public:

		/*
		* Constructor
		*/
		Input_Validator_Weather(Input_Validator_Base* baseValidator) : inputValidator(baseValidator) {};

		bool isValid_WindDirection(std::string input);
		bool isValid_StationName(std::string input);
		bool isValid_TempDegrees(std::string input);
		bool isValid_WindSpeed(std::string input);
};

#endif
