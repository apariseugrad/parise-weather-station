#include "stdafx.h"
#include "PropertiesReader.h"
#include <iostream>
#include <fstream>
#include <windows.h>


PropertiesMap Properties_Reader::getPropertiesMap() {
	if (this->propMap != nullptr) {
		return *this->propMap;
	}
	else {
		//if the propMap is a nullptr, then the properties have not been successfully parsed
		if (this->tryParsePropertiesFile() && this->propMap != nullptr) {
			//if the parsing was successfull and the propMap was set
			return *this->propMap;
		}
		else {
			//something went wrong with the parsing

		}
	}
}

std::string * Properties_Reader::getPropertyValue(std::string * key)
{
	if (key != nullptr) {
		if (this->containsKey(key)) {
			PropertiesMap properties = this->getPropertiesMap();
			return new std::string(properties[*key]);
		}
	}
	return new std::string("");
}

bool Properties_Reader::containsKey(std::string * key)
{
	if (key != nullptr) {
		PropertiesMap properties = this->getPropertiesMap();

		if (properties[*key].empty()) {
			return false;
		}
		else {
			return true;
		}
	}

	return false;
}


std::string Properties_Reader::getFileName() {
	if (this->fileName != nullptr) {
		return *this->fileName;
	}
	return "";
}

void Properties_Reader::setFileName(std::string newFileName) {
	if (this->fileName != nullptr) {
		delete(this->fileName);
	}

	this->fileName = new std::string(newFileName);
}


void Properties_Reader::printAllProperties()
{
	PropertiesMap properties = this->getPropertiesMap();
	std::cout << "All settings:\n";
	for (PropertiesMap::iterator i = properties.begin(); i != properties.end(); i++) {
		std::cout << "Item \"" << i->first << "\" has value \"" << i->second << '\"' << std::endl;
	}

}

bool Properties_Reader::tryParsePropertiesFile() {
	PropertiesMap properties;
	std::string line;
	std::string fileName = this->getFileName();
	std::ifstream fileStream(fileName, std::iostream::in);

	if (fileStream) {
		while (std::getline(fileStream, line))
		{
			std::istringstream is_line(line);
			std::string key;

			if (std::getline(is_line, key, '='))
			{
				std::string value;
				if (key[0] == '#') {
					//ignore comments
					continue;
				}

				if (std::getline(is_line, value))
				{
					properties[key] = value;
				}
			}
		}
		//File Parsing Complete
		this->propMap = new PropertiesMap(properties);
		return true;
	}
	else {
		//File Stream failed to open
		return false;
	}
	
	
}



