#pragma once

#ifndef PROPERTIES_READER_H
#define PROPERTIES_READER_H

#include "stdafx.h"
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>

typedef std::map<const std::string, std::string> PropertiesMap;

class Properties_Reader {

	std::string* fileName;
	PropertiesMap* propMap;


public:

	/*
	* Constructor
	*/
	Properties_Reader(std::string* fileName) : fileName(fileName), propMap(nullptr) {};

	/*
	 * Operations
	 */
	PropertiesMap getPropertiesMap();

	/*
	 * @return the value associated with @param key
	 */
	std::string* getPropertyValue(std::string* key);

	/*
	* @return a boolean representing whether the key exists or not in the properties map
	*/
	bool containsKey(std::string* key);

	/*
	 * Getters/Setters
	 */
	std::string getFileName();
	void setFileName(std::string fileName);

	/*
	* Prints all key value pairs of the properties file to the console
	* Note: Debugging function; no production use case
	*/
	void printAllProperties();

private:

	/*
	 * Attempts to parse the properties file with the set values
	 * @return bool respresenting the success case
	 */
	bool tryParsePropertiesFile();

	
};

#endif
