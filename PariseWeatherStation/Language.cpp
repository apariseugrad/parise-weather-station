#include "Language.h"

const char * getTextForLanguage(int langVal)
{
	return LanguageStrings[langVal - 1];
}

const char * getDataFileForLanguage(int langVal)
{
	return DataFileStrings[langVal -1];
}
