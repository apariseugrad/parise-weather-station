#pragma once

#ifndef WEATHER_READING_H
#define WEATHER_READING_H

#include "stdafx.h"
#include <string>

#include "temperature.h"
#include "wind.h"


class Weather_Reading {

	Wind* wind;
	Temperature* temp;

	public:

		/*
		* Constructor
		*/
		Weather_Reading() : wind(nullptr), temp(nullptr) {};
	

		/*
		* Getters/Setters
		*/

		Temperature getTemp();
		void setTemp(Temperature newTemp);

		Wind getWind();
		void setWind(Wind newWindSpeed);

};


#endif
