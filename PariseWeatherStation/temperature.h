#pragma once

#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include "stdafx.h"

class Temperature {

	int* degrees;

	public:

		/*
		* Constructor
		*/
		Temperature() : degrees(nullptr) {};

		/*
		* Getters/Setters
		*/
		int getDegrees();
		void setDegrees(int newDegrees);

};


#endif
