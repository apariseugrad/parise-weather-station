#include "stdafx.h"
#include "temperature.h"

int Temperature::getDegrees() {
	if (this->degrees != nullptr) {
		return *this->degrees;
	}
}

void Temperature::setDegrees(int newDegrees) {
	if (this->degrees != nullptr) {
		delete(this->degrees);
	}

	this->degrees = new int(newDegrees);
}

