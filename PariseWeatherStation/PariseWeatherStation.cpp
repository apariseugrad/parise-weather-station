// PariseWeatherStation.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream> 
#include <string>
#include <cstdlib>
#include <conio.h>
#include <sstream> 
#include <stdlib.h>
#include <cstdio>
#include <vector>
#include <fstream>
#include <regex>
#include <stack>

#include "inputvalidatorbase.h"
#include "inputvalidatorweather.h"
#include "weatherreading.h"
#include "weatherstation.h"
#include "wind.h"
#include "temperature.h"
#include "PropertiesReader.h"
#include "Language.h"


using namespace std;


const int BUFFER_SIZE = 100;
typedef char buffer[BUFFER_SIZE];

int displayMenu(Input_Validator_Base* baseValidator, Weather_Station* weatherStation);

/** Util_Functions */

bool in_array(const std::string &value, const vector<string> &array)
{
	return std::find(array.begin(), array.end(), value) != array.end();
}
/** !Util_Functions */


/* !Input_Validation */

/* Data_Normalization */

string normalizeWindDirection(string inputString) {
	vector<string> possibleValues_East{ "e", "E", "East", "east", "EAST" };
	vector<string> possibleValues_West{ "w", "W", "west", "West", "WEST" };
	vector<string> possibleValues_North{ "n", "north", "NORTH", "N", "North" };
	vector<string> possibleValues_South{ "s", "S", "SOUTH", "south", "South" };
	vector<string> possibleValues_Northeast{ "NE", "ne", "Ne", "nE", "North East", "NORTHEAST", "northeast", "north east", "North east", "north East" };
	vector<string> possibleValues_Southeast{ "SE", "se", "Se", "sE", "South East", "SOUTHEAST", "southeast", "south east", "South east", "south East" };
	vector<string> possibleValues_Southwest{ "SW", "sw", "Sw", "sW", "South West", "SOUTHWEST", "southwest", "south west", "South west", "south West" };
	vector<string> possibleValues_Northwest{ "NW", "nw", "Nw", "nW", "North West", "NORTHWEST", "northwest", "north west", "North west", "north West" };
	
	string normalized_East = "East";
	string normalized_West = "West";
	string normalized_North = "North";
	string normalized_South = "South";
	string normalized_Northeast = "Northeast";
	string normalized_Northwest = "Northwest";
	string normalized_Southeast = "Southeast";
	string normalized_Southwest = "Southwest";

	if (in_array(inputString, possibleValues_East)) {
		return normalized_East;
	}
	if (in_array(inputString, possibleValues_West)) {
		return normalized_West;
	}
	if (in_array(inputString, possibleValues_North)) {
		return normalized_West;
	}
	if (in_array(inputString, possibleValues_South)) {
		return normalized_South;
	}
	if (in_array(inputString, possibleValues_Southwest)) {
		return normalized_Southwest;
	}
	if (in_array(inputString, possibleValues_Northwest)) {
		return normalized_Northwest;
	}
	if (in_array(inputString, possibleValues_Northeast)) {
		return normalized_Northeast;
	}
	if (in_array(inputString, possibleValues_Southeast)) {
		return normalized_Southeast;
	}

	return "";
}

/* !Data_Normalization */

/* Data_Output */


void displayReading(Weather_Station* weatherStation, bool onlyMostRecent = true)
{
	string * message_StationName_1Key = new string("message_StationName_1");
	string * message_StationName_1Value = weatherStation->getOutputMessage(message_StationName_1Key);

	string * message_StationName_2Key = new string("message_StationName_2");
	string * message_StationName_2Value = weatherStation->getOutputMessage(message_StationName_2Key);

	string * message_CurTemp_1Key = new string("message_CurTemp_1");
	string * message_CurTemp_1Value = weatherStation->getOutputMessage(message_CurTemp_1Key);

	string * message_CurTemp_2Key = new string("message_CurTemp_2");
	string * message_CurTemp_2Value = weatherStation->getOutputMessage(message_CurTemp_2Key);

	string * message_CurWind_1Key = new string("message_CurWind_1");
	string * message_CurWind_1Value = weatherStation->getOutputMessage(message_CurWind_1Key);

	string * message_CurWind_2Key = new string("message_CurWind_2");
	string * message_CurWind_2Value = weatherStation->getOutputMessage(message_CurWind_2Key);

	string * message_ContinueKey = new string("message_Continue");
	string * message_ContinueValue = weatherStation->getOutputMessage(message_ContinueKey);

	string * message_ContinueNextReadingKey = new string("message_ContinueNextReading");
	string * message_ContinueNextReadingValue = weatherStation->getOutputMessage(message_ContinueNextReadingKey);

	string * message_ReadingNo_1Key = new string("message_ReadingNo_1");
	string * message_ReadingNo_1Value = weatherStation->getOutputMessage(message_ReadingNo_1Key);

	string * message_ReadingNo_2Key = new string("message_ReadingNo_2");
	string * message_ReadingNo_2Value = weatherStation->getOutputMessage(message_ReadingNo_2Key);

	string * message_Temp_1Key = new string("message_Temp_1");
	string * message_Temp_1Value = weatherStation->getOutputMessage(message_Temp_1Key);

	string * message_Temp_2Key = new string("message_Temp_2");
	string * message_Temp_2Value = weatherStation->getOutputMessage(message_Temp_2Key);

	string * message_Wind_1Key = new string("message_Wind_1");
	string * message_Wind_1Value = weatherStation->getOutputMessage(message_Wind_1Key);

	string * message_Wind_2Key = new string("message_Wind_2");
	string * message_Wind_2Value = weatherStation->getOutputMessage(message_Wind_2Key);


	cout << endl;
	cout << *message_StationName_1Value << weatherStation->getStationName() << *message_StationName_2Value << endl;
	cout << endl;
	stack<Weather_Reading*> readings = weatherStation->getReadings();
	Weather_Reading * curReading = readings.top();
	if (curReading != nullptr && onlyMostRecent) {

		cout << *message_CurTemp_1Value << curReading->getTemp().getDegrees() << *message_CurTemp_2Value << endl;
		cout << *message_CurWind_1Value << curReading->getWind().getWindSpeed() << *message_CurWind_2Value << curReading->getWind().getWindDirection() << endl;
		cout << endl;
		cout << *message_ContinueValue << endl;
		_getch();
		return;
	}
	else {
		//Display Whole History

		int curReadingNum = 0;
		while (curReadingNum < weatherStation->getHistorySize() && curReading != nullptr) {
			//Display Data
			cout << *message_ReadingNo_1Value << (curReadingNum + 1) << *message_ReadingNo_2Value << endl;
			cout << *message_Temp_1Value << curReading->getTemp().getDegrees() << *message_Temp_2Value << endl;
			cout << *message_Wind_1Value << curReading->getWind().getWindSpeed() << *message_Wind_2Value << curReading->getWind().getWindDirection() << endl;
			cout << endl;
			
			//Increment counter/pointer
			curReadingNum++;
			readings.pop();
			if (readings.empty()) {
				//if there is no more readings
				cout << *message_ContinueValue << endl;
				_getch();
				break;
			}
			else {
				curReading = readings.top();
				if (curReading == nullptr || curReadingNum >= weatherStation->getHistorySize()) {
					//if there is nullptr or we have reached the max number to display
					cout << *message_ContinueValue << endl;
					_getch();
					break;
				}
				else {
					cout << *message_ContinueNextReadingValue << endl;
					_getch();
				}
			}
		}
	}
}

/* !Data_Output */

/* Data_Input */

string getConsoleInput() {
	string userInputStr;
	buffer userInput;
	cin.getline(userInput, BUFFER_SIZE);
	userInputStr = userInput;
	return userInputStr;
}

string enterWeatherStationName(Input_Validator_Weather* weatherValidator, Weather_Station* weatherStation) {
	string * enterStationNameMessageKey = new string("enterStationNameMessage");
	string * enterStationNameMessageValue = weatherStation->getOutputMessage(enterStationNameMessageKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	while (1) {
		cout << *enterStationNameMessageValue << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!weatherValidator->isValid_StationName(userInput)) {
			continue;
		}

		return userInput;
	}
}

int enterTemperatureDegrees(Input_Validator_Weather* weatherValidator, Weather_Station* weatherStation) {
	string * enterTemperatureMessageKey = new string("enterTemperatureMessage");
	string * enterTemperatureMessageValue = weatherStation->getOutputMessage(enterTemperatureMessageKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	while (1) {
		cout << *enterTemperatureMessageValue << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!weatherValidator->isValid_TempDegrees(userInput)) {
			continue;
		}

		int userInputInt = 0;

		stringstream(userInput) >> userInputInt;

		return userInputInt;
	}

}

int enterWindSpeed(Input_Validator_Base* baseValidator, Weather_Station* weatherStation) {
	string * enterWindSpeedMessageKey = new string("enterWindSpeedMessage");
	string * enterWindSpeedMessageValue = weatherStation->getOutputMessage(enterWindSpeedMessageKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	string * inputValidationErrorMessage_signKey = new string("inputValidationErrorMessage_sign");
	string * inputValidationErrorMessage_signValue = weatherStation->getOutputMessage(inputValidationErrorMessage_signKey);

	while (1) {
		cout << *enterWindSpeedMessageValue << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!baseValidator->isValid_UnsignedInt(userInput)) {
			continue;
		}

		int userInputInt = 0;

		stringstream(userInput) >> userInputInt;
		if (userInputInt < 0) {
			cout << *inputValidationErrorMessage_signValue << endl;
			continue;
		}

		return userInputInt;
	}

}

int enterHistorySize(Input_Validator_Base* baseValidator, Weather_Station* weatherStation) {

	string * enterHistorySizeMessageKey = new string("enterHistorySizeMessage");
	string * enterHistorySizeMessageValue = weatherStation->getOutputMessage(enterHistorySizeMessageKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	string * inputValidationErrorMessage_signKey = new string("inputValidationErrorMessage_sign");
	string * inputValidationErrorMessage_signValue = weatherStation->getOutputMessage(inputValidationErrorMessage_signKey);

	string * inputValidationErrorMessage_tooLargeKey = new string("inputValidationErrorMessage_tooLarge");
	string * inputValidationErrorMessage_tooLargeValue = weatherStation->getOutputMessage(inputValidationErrorMessage_tooLargeKey);


	while (1) {
		cout << *enterHistorySizeMessageValue << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!baseValidator->isValid_UnsignedInt(userInput)) {
			continue;
		}

		int userInputInt = 0;

		stringstream(userInput) >> userInputInt;
		if (userInputInt < 0) {
			cout << *inputValidationErrorMessage_signValue << endl;
			continue;
		}
		if (userInputInt >= 100) {
			cout << *inputValidationErrorMessage_tooLargeValue << endl;
			continue;
		}

		return userInputInt;
	}

}

string enterWindDirection(Input_Validator_Base* baseValidator, Weather_Station* weatherStation) {
	string * enterWindDirectionMessageKey = new string("enterWindDirectionMessage");
	string * enterWindDirectionMessageValue = weatherStation->getOutputMessage(enterWindDirectionMessageKey);
	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	while (1) {
		cout << *enterWindDirectionMessageValue << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!baseValidator->isValid_Direction(userInput)) {
			continue;
		}
		return normalizeWindDirection(userInput);
	}

}

/* !Data_Input */

/*
 * Displays the main menu -- returns the integer associated with the selected menu option
 *
 */
int displayMenu(Input_Validator_Base* baseValidator, Weather_Station* weatherStation) {
	//reader->getPropertyValue(message_NoReadingsErrorKey);
	string * asteriskBorderKey = new string("asteriskBorder");
	string * asteriskBorderValue = weatherStation->getOutputMessage(asteriskBorderKey);

	string * weatherMenuGreetingKey = new string("weatherMenuGreeting");
	string * weatherMenuGreetingValue = weatherStation->getOutputMessage(weatherMenuGreetingKey);

	string * menuOptionDisplayKey = new string("menuOptionDisplay");
	string * menuOptionDisplayValue = weatherStation->getOutputMessage(menuOptionDisplayKey);

	string * menuOptionDisplayHistoryKey = new string("menuOptionDisplayHistory");
	string * menuOptionDisplayHistoryValue = weatherStation->getOutputMessage(menuOptionDisplayHistoryKey);

	string * menuOptionInputKey = new string("menuOptionInput");
	string * menuOptionInputValue = weatherStation->getOutputMessage(menuOptionInputKey);

	string * menuOptionLanguageKey = new string("menuOptionLanguage");
	string * menuOptionLanguageValue = weatherStation->getOutputMessage(menuOptionLanguageKey);

	string * menuOptionExitKey = new string("menuOptionExit");
	string * menuOptionExitValue = weatherStation->getOutputMessage(menuOptionExitKey);

	string * promptInputKey = new string("promptInput");
	string * promptInputValue = weatherStation->getOutputMessage(promptInputKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	string * inputValidationErrorMessageKey = new string("inputValidationErrorMessage");
	string * inputValidationErrorMessageValue = weatherStation->getOutputMessage(inputValidationErrorMessageKey);


	while (1) {
		
		cout << endl;
		cout << *asteriskBorderValue << endl;
		cout << *weatherMenuGreetingValue << endl;
		cout << *asteriskBorderValue << endl;
		cout << *menuOptionDisplayValue << endl;
		cout << *menuOptionDisplayHistoryValue << endl;
		cout << *menuOptionInputValue << endl;
		cout << *menuOptionLanguageValue << endl;
		cout << *menuOptionExitValue << endl;
		cout << *promptInputValue;
		cout << endl;
		cout << *cursorValue;
		
		string userInput = getConsoleInput();

		if (!baseValidator->isValid_Integer(userInput, false)) {
			continue;
		}

		int userInputInt = 0;

		stringstream(userInput) >> userInputInt;

		switch (userInputInt) {
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 3;
		case 4:
			return 4;
		case 5:
			return 5;
		default:
			cout << *inputValidationErrorMessageValue << endl;
			break;
		}
	}
	return 0;
}

Language displayLanguageMenu(Input_Validator_Base* baseValidator, Weather_Station* weatherStation) {
	string * asteriskBorderKey = new string("asteriskBorder");
	string * asteriskBorderValue = weatherStation->getOutputMessage(asteriskBorderKey);

	string * languageMenuGreetingKey = new string("languageMenuGreeting");
	string * languageMenuGreetingValue = weatherStation->getOutputMessage(languageMenuGreetingKey);

	string * langMenuOptionEnglishKey = new string("langMenuOptionEnglish");
	string * langMenuOptionEnglishValue = weatherStation->getOutputMessage(langMenuOptionEnglishKey);

	string * langMenuOptionRussianKey = new string("langMenuOptionRussian");
	string * langMenuOptionRussianValue = weatherStation->getOutputMessage(langMenuOptionRussianKey);

	string * promptInputKey = new string("promptInput");
	string * promptInputValue = weatherStation->getOutputMessage(promptInputKey);

	string * cursorKey = new string("cursor");
	string * cursorValue = weatherStation->getOutputMessage(cursorKey);

	string * inputValidationErrorMessageKey = new string("inputValidationErrorMessage");
	string * inputValidationErrorMessageValue = weatherStation->getOutputMessage(inputValidationErrorMessageKey);


	while (1) {

		cout << endl;
		cout << *languageMenuGreetingValue << endl;
		cout << *langMenuOptionEnglishValue << endl;
		cout << *langMenuOptionRussianValue << endl;
		cout << *promptInputValue;
		cout << endl;
		cout << *cursorValue;

		string userInput = getConsoleInput();

		if (!baseValidator->isValid_Integer(userInput, false)) {
			continue;
		}

		int userInputInt = 0;

		stringstream(userInput) >> userInputInt;

		switch (userInputInt) {
		case 1:
			return Language::English;
		case 2:
			return Language::Russian;
		default:
			cout << *inputValidationErrorMessageValue << endl;
			break;
		}
	}
	return Language::English;
}

int main()
{
	Weather_Station* weatherStation = new Weather_Station();
	weatherStation->setLanguage(Language::English); //Set language to English at first

	string * message_BannerBorderKey = new string("message_BannerBorder");
	string * message_BannerBorderValue = weatherStation->getOutputMessage(message_BannerBorderKey);

	string * message_BannerContentKey = new string("message_BannerContent");;
	string * message_BannerContentValue = weatherStation->getOutputMessage(message_BannerContentKey);

	string * message_NoReadingsErrorKey = new string("message_NoReadingsError");
	string * message_NoReadingsErrorValue = weatherStation->getOutputMessage(message_NoReadingsErrorKey);

	cout << *message_BannerBorderValue << endl;
	cout << *message_BannerContentValue << endl;
	cout << *message_BannerBorderValue << endl;
	
	Input_Validator_Base* baseValidator = new Input_Validator_Base(new bool(true));
	Input_Validator_Weather* weatherValidator = new Input_Validator_Weather(baseValidator);

	while (1) {

		int selectedMenuOption = displayMenu(baseValidator, weatherStation);

		switch (selectedMenuOption) 
		{
			case 1: 
				//Display Reading
				if (weatherStation->hasReadings()) {
					displayReading(weatherStation, true);
				}
				else {
					cout << *message_NoReadingsErrorValue << endl;
				}
				break;
			case 2:
				//Display History
				if (weatherStation->hasReadings()) {
					displayReading(weatherStation, false);
				}
				else {
					cout << *message_NoReadingsErrorValue << endl;
				}
				break;
			case 3:
			{
				//Input Reading
				if (!weatherStation->hasReadings()) {
					if (!weatherStation->hasLanguagePrompted()) {
						//Language Setting - we only want to enter this once
						weatherStation->setLanguage(displayLanguageMenu(baseValidator, weatherStation));
					}

					//stationName - only want to enter it once
					weatherStation->setStationName(enterWeatherStationName(weatherValidator, weatherStation));
					weatherStation->setHistorySize(enterHistorySize(baseValidator, weatherStation));
				}

				Weather_Reading* newReadingPtr = new Weather_Reading();

				//temperature
				Temperature* newTemp = new Temperature();
				newTemp->setDegrees(enterTemperatureDegrees(weatherValidator, weatherStation));
				newReadingPtr->setTemp(*newTemp);

				//wind
				Wind* newWind = new Wind();
				///wind speed
				newWind->setWindSpeed(enterWindSpeed(baseValidator, weatherStation));

				///wind direction
				newWind->setWindDirection(enterWindDirection(baseValidator, weatherStation));
				newReadingPtr->setWind(*newWind);

				weatherStation->storeReading(newReadingPtr);
				break;
				}
			case 4:
				//Change language
				weatherStation->setLanguage(displayLanguageMenu(baseValidator, weatherStation));
				break;
			case 5: 
				//Exit
				return 0;
		}
	}

	return 0;
}
