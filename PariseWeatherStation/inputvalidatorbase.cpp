#include "stdafx.h"
#include <string>
#include <regex>
#include <iostream>
#include <sstream> 

#include "inputvalidatorbase.h"


bool Input_Validator_Base::doPrintErrorMessages() {
	if (this->displayErrorMessages != nullptr) {
		return *this->displayErrorMessages;
	}
	else {
		return false;
	}
}
void Input_Validator_Base::toggleDisplayErrorMessages(bool newDisplayErrorMesagesValue) {
	if (this->displayErrorMessages != nullptr) {
		delete(this->displayErrorMessages);
	}
	this->displayErrorMessages = new bool(newDisplayErrorMesagesValue);
}

bool Input_Validator_Base::valid_input(std::string input, std::string validation) {
	const char* test = input.c_str();
	std::regex re(validation);
	std::cmatch match;
	if (regex_match(test, match, re)) {
		return true;
	}
	else {
		return false;
	}
}

bool Input_Validator_Base::isValid_Integer(std::string input, bool isSigned) {
	std::string failMessage1_1 = "System was unable to process input. Value was too large. Please enter an integer less than ";
	std::string failMessage1_2 = " digits";
	std::string regex_SignedDigit = "^-?[[:digit:]]+$";

	std::string failMessage2_1 = "System was unable to process input. Expected '+' or '-' followed by an integer.";
	std::string regex_Digit = "[[:digit:]]+";

	std::string failMessage3_1 = "System was unable to process input. Expected an integer.";

	if (isSigned) {
		if (this->valid_input(input, regex_SignedDigit)) {
			//Check to see if the input is greater than what the integer data type can hold
			if (input.size() >= INT_MAX_DIGITS) {
				if (this->doPrintErrorMessages()) {
					std::stringstream ss;
					ss << failMessage1_1 << INT_MAX_DIGITS << failMessage1_2;
					std::string errorMessage = ss.str();
					std::cout << errorMessage << std::endl;
				}
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (this->doPrintErrorMessages()) {
				std::cout << failMessage2_1 << std::endl;
			}

			return false;
		}
	}
	else {
		if (this->valid_input(input, regex_Digit)) {
			if (input.length() >= INT_MAX_DIGITS) {
				if (this->doPrintErrorMessages()) {
					std::stringstream ss;
					ss << failMessage1_1 << INT_MAX_DIGITS << failMessage1_2;
					std::string errorMessage = ss.str();
					std::cout << errorMessage << std::endl;
				}
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (this->doPrintErrorMessages()) {
				std::cout << failMessage3_1 << std::endl;
			}
			return false;
		}
	}
}

bool Input_Validator_Base::isValid_ContainsOnlyLetters(std::string input) {

	std::string failMessage1 = "System was unable to process input. Expected a series of letters.";
	std::string regex_letter = "[a-zA-Z]*";

	if (this->valid_input(input, regex_letter)) {
		return true;
	}
	else {
		if (this->doPrintErrorMessages()) {
			std::cout << failMessage1 << std::endl;
		}
		return false;
	}
}

bool Input_Validator_Base::isValid_NonEmptyString(std::string input) {
	std::string failMessage1 = "System was unable to process input. Expected a non-empty input.";
	if (!input.empty()) {
		return true;
	}
	else {
		if (this->doPrintErrorMessages()) {
			std::cout << failMessage1 << std::endl;
		}
		return false;
	}
}

bool Input_Validator_Base::isValid_UnsignedInt(std::string input) {
	if (this->isValid_Integer(input, false)) {
		return true;
	}
	return false;
}

bool Input_Validator_Base::isValid_Direction(std::string input) {
	std::string failMessage1 = "System was unable to process input. Expected either 'N', 'E', 'S', 'SE', 'SW', 'W', 'NW', or 'NE'.";
	std::string regex_Direction = R"((\b(?:north|south|North|South|NORTH|SOUTH)(?:(?:east|west|East|West|EAST|WEST))?\b|\b(?:east|west|West|East|WEST|EAST|[NS][WE]?|[EW])\b))";
	if (this->valid_input(input, regex_Direction)) {
		return true;
	}
	else {
		if (this->doPrintErrorMessages()) {
			std::cout << failMessage1 << std::endl;
		}
		return false;
	}
}
