#pragma once

#ifndef INPUT_VALIDATOR_BASE_H
#define INPUT_VALIDATOR_BASE_H

#include "stdafx.h"
#include <string>

const int INT_MAX_DIGITS = 9;

class Input_Validator_Base {

	bool* displayErrorMessages;

	public:

		/*
		 * Constructor
		 */
		Input_Validator_Base(bool* doPrintErrorMsgs) : displayErrorMessages(doPrintErrorMsgs) {};

		bool doPrintErrorMessages();
		void toggleDisplayErrorMessages(bool newDisplayErrorMesagesValue);
		bool valid_input(std::string input, std::string validation);
		bool isValid_Integer(std::string input, bool isSigned);
		bool isValid_ContainsOnlyLetters(std::string input);
		bool isValid_NonEmptyString(std::string input);
		bool isValid_UnsignedInt(std::string input);
		bool isValid_Direction(std::string input);

};

#endif
