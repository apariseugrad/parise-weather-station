#include "stdafx.h"
#include "weatherreading.h"
#include <string>

Temperature Weather_Reading::getTemp() {
	if (this->temp != nullptr) {
		return *this->temp;
	}
}

void Weather_Reading::setTemp(Temperature newTemp) {
	if (this->temp != nullptr) {
		delete(this->temp);
	}

	this->temp = new Temperature(newTemp);
}

Wind Weather_Reading::getWind() {
	if (this->wind != nullptr) {
		return *this->wind;
	}
}

void Weather_Reading::setWind(Wind newWind) {
	if (this->wind != nullptr) {
		delete(this->wind);
	}

	this->wind = new Wind(newWind);
}
