#include "stdafx.h"
#include "weatherstation.h"

int Weather_Station::numReadings() { 
	return readings.size(); 
}

Weather_Reading* Weather_Station::latestReading() {
	return readings.top(); 
}

std::string * Weather_Station::getOutputMessage(std::string * messageKey)
{
	if (this->properties != nullptr) {
		return this->properties->getPropertyValue(messageKey);
	}
	else {
		return new std::string("");
	}
}

bool Weather_Station::hasReadings() {
	return !readings.empty(); 
}

void Weather_Station::storeReading(Weather_Reading* newReading) {
	this->readings.push(newReading);
}

int Weather_Station::getHistorySize() {
	if (this->historySize != nullptr) {
		return *this->historySize;
	}
}

void Weather_Station::setHistorySize(int newHistorySize) {
	if (this->historySize != nullptr) {
		delete(this->historySize);
	}
	this->historySize = new int(newHistorySize);
}

std::string Weather_Station::getStationName() {
	if (this->stationName != nullptr) {
		return *this->stationName;
	}
}

void Weather_Station::setStationName(std::string newStationName) {
	if (this->stationName != nullptr) {
		delete(this->stationName);
	}

	this->stationName = new std::string(newStationName);
}

std::stack <Weather_Reading*> Weather_Station::getReadings() {
	return this->readings;
}

bool Weather_Station::hasLanguagePrompted()
{
	return this->hasSetLanguage;
}

Language Weather_Station::getLanguage() {
	if (this->language != nullptr) {
		return *this->language;
	}
}

void Weather_Station::setLanguage(Language newLanguage) {
	if (this->language != nullptr) {
		//This code path only hit when replacing the set language -- The language is set to English on start, so this is hit when the user changes the language
		this->hasSetLanguage = true;
		delete(this->language);
	}
	if (this->properties != nullptr) {
		delete(this->properties);
	}

	this->language = new Language(newLanguage);

	this->properties = new Properties_Reader(new std::string(getDataFileForLanguage(newLanguage)));
	properties->getPropertiesMap();
}