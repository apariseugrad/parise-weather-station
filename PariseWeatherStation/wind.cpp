#include "stdafx.h"
#include "wind.h"
#include <string>


int Wind::getWindSpeed() {
	if (this->windSpeed != nullptr) {
		return *this->windSpeed;
	}
}

void Wind::setWindSpeed(int newWindSpeed) {
	if (this->windSpeed != nullptr) {
		delete(this->windSpeed);
	}

	this->windSpeed = new int(newWindSpeed);
}

std::string Wind::getWindDirection() {
	if (this->windDir != nullptr) {
		return *this->windDir;
	}
}

void Wind::setWindDirection(std::string newWindDirection) {
	if (this->windDir != nullptr) {
		delete(this->windDir);
	}

	this->windDir = new std::string(newWindDirection);
}
